package com.steven.frio.coolbeanslibrarycafeadmin.recyclerparsers;

/**
 * Created by steven on 9/12/17.
 */

public class RParserMain {

    private Integer orderId;
    private String orderName;
    private int custId;
    private String orderCategory;
    private String orderSize;
    private int quantity;

    public RParserMain(Integer orderId, String orderName, int custId, String orderCategory, String orderSize, int quantity) {
        this.orderId = orderId;
        this.orderName = orderName;
        this.custId = custId;
        this.orderCategory = orderCategory;
        this.orderSize = orderSize;
        this.quantity = quantity;
    }

    public void setOrderId(Integer orderId){
        this.orderId = orderId;
    }

    public Integer getOrderId(){
        return orderId;
    }

    public void setOrderName(String orderName){
        this.orderName = orderName;
    }

    public String getOrderName(){
        return orderName;
    }


    public int getCustId() {
        return custId;
    }

    public void setCustId(int custId) {
        this.custId = custId;
    }

    public String getOrderCategory() {
        return orderCategory;
    }

    public void setOrderCategory(String orderCategory) {
        this.orderCategory = orderCategory;
    }

    public String getOrderSize() {
        return orderSize;
    }

    public void setOrderSize(String orderSize) {
        this.orderSize = orderSize;
    }

    public int getQuantity() {
        return quantity;
    }

    public void setQuantity(int quantity) {
        this.quantity = quantity;
    }
}
