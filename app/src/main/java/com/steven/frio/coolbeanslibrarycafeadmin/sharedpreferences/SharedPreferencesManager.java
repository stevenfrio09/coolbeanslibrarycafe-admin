package com.steven.frio.coolbeanslibrarycafeadmin.sharedpreferences;

import android.content.Context;
import android.content.SharedPreferences;
import android.util.Log;

import com.steven.frio.coolbeanslibrarycafeadmin.configs.StringConfig;

/**
 * Created by steven on 10/3/17.
 */

public class SharedPreferencesManager {

    private Context context;

    private SharedPreferences sharedPreferencesListUser;
    private SharedPreferences sharedPreferencesFoodMain;
    private SharedPreferences sharedPreferencesFoodCategory;
    private SharedPreferences sharedPreferencesFoodServing;
    private SharedPreferences sharedPreferencesDrinkMain;
    private SharedPreferences sharedPreferencesDrinkCategory;
    private SharedPreferences sharedPreferencesDrinkServing;
    private SharedPreferences sharedPreferencesOrderQueueFood;
    private SharedPreferences sharedPreferencesOrderQueueDrink;

    private SharedPreferences.Editor editorListUser;
    private SharedPreferences.Editor editorFoodMain;
    private SharedPreferences.Editor editorFoodCategory;
    private SharedPreferences.Editor editorFoodServing;
    private SharedPreferences.Editor editorDrinkMain;
    private SharedPreferences.Editor editorDrinkCategory;
    private SharedPreferences.Editor editorDrinkServing;
    private SharedPreferences.Editor editorOrderQueueFood;
    private SharedPreferences.Editor editorOrderQueueDrink;


    static SharedPreferences sharedPreferencesAdmin;
    static SharedPreferences.Editor editorAdmin;

    public SharedPreferencesManager(Context context) {
        this.context = context;

        sharedPreferencesListUser = context.getSharedPreferences(StringConfig.SHAREDPREF_LIST_USER, 0);

        sharedPreferencesFoodMain = context.getSharedPreferences(StringConfig.SHAREDPREF_LIST_FOOD_MAIN, 0);
        sharedPreferencesFoodCategory = context.getSharedPreferences(StringConfig.SHAREDPREF_LIST_FOOD_CATEGORY, 0);
        sharedPreferencesFoodServing = context.getSharedPreferences(StringConfig.SHAREDPREF_LIST_FOOD_SERVING, 0);
        sharedPreferencesDrinkMain = context.getSharedPreferences(StringConfig.SHAREDPREF_LIST_DRINK_MAIN, 0);
        sharedPreferencesDrinkCategory = context.getSharedPreferences(StringConfig.SHAREDPREF_LIST_DRINK_CATEGORY, 0);
        sharedPreferencesDrinkServing = context.getSharedPreferences(StringConfig.SHAREDPREF_LIST_DRINK_SERVING, 0);

        sharedPreferencesOrderQueueFood = context.getSharedPreferences(StringConfig.SHAREDPREF_ORDERQUEUE_FOOD,0);
        sharedPreferencesOrderQueueDrink = context.getSharedPreferences(StringConfig.SHAREDPREF_ORDERQUEUE_DRINK,0);

        sharedPreferencesAdmin = context.getSharedPreferences(StringConfig.SHAREDPREF_ADMIN, 0);

        editorListUser = sharedPreferencesListUser.edit();
        editorFoodMain = sharedPreferencesFoodMain.edit();
        editorFoodCategory = sharedPreferencesFoodCategory.edit();
        editorFoodServing = sharedPreferencesFoodServing.edit();

        editorDrinkMain = sharedPreferencesDrinkMain.edit();
        editorDrinkCategory = sharedPreferencesDrinkCategory.edit();
        editorDrinkServing = sharedPreferencesDrinkServing.edit();

        editorOrderQueueFood = sharedPreferencesOrderQueueFood.edit();
        editorOrderQueueDrink = sharedPreferencesOrderQueueDrink.edit();

        editorAdmin = sharedPreferencesAdmin.edit();

    }

    // set shared pref user list
    public void setEditorListUser(String key, String value) {
        editorListUser.putString(key, value);
        editorListUser.apply();
    }

    public void setEditorFoodMain(String key, String value) {
        editorFoodMain.putString(key, value);
        editorFoodMain.apply();
    }

    public void setEditorFoodCategory(String key, String value) {
        editorFoodCategory.putString(key, value);
        editorFoodCategory.apply();
    }

    public void setEditorFoodServing(String key, String value) {
        editorFoodServing.putString(key, value);
        editorFoodServing.apply();
    }

    public void setEditorDrinkMain(String key, String value) {
        editorDrinkMain.putString(key, value);
        editorDrinkMain.apply();
    }

    public void setEditorDrinkCategory(String key, String value) {
        editorDrinkCategory.putString(key, value);
        editorDrinkCategory.apply();
    }

    public void setEditorDrinkServing(String key, String value) {
        editorDrinkServing.putString(key, value);
        editorDrinkServing.apply();
    }


    public void setEditorOrderQueueFood(String key, String value){
        editorOrderQueueFood.putString(key, value);
        editorOrderQueueFood.apply();
    }

    public void setEditorOrderQueueDrink(String key, String value){
        editorOrderQueueDrink.putString(key, value);
        editorOrderQueueDrink.apply();
    }

    // get shared pref user list
    public String getSharedPreferencesListUser(String s) {
        return sharedPreferencesListUser.getString(s, null);
    }

    public String getSharedPreferencesFoodMain(String s) {
        return sharedPreferencesFoodMain.getString(s, null);
    }

    public String getSharedPreferencesFoodCategory(String s) {
        return sharedPreferencesFoodCategory.getString(s, null);
    }

    public String getSharedPreferencesFoodServing(String s) {
        return sharedPreferencesFoodServing.getString(s, null);
    }

    public String getSharedPreferencesDrinkMain(String s) {
        return sharedPreferencesDrinkMain.getString(s, null);
    }

    public String getSharedPreferencesDrinkCategory(String s) {
        return sharedPreferencesDrinkCategory.getString(s, null);
    }

    public String getSharedPreferencesDrinkServing(String s) {
        return sharedPreferencesDrinkServing.getString(s, null);
    }

    public String getSharedPreferenceOrderQueueFood(String s){
        return sharedPreferencesOrderQueueFood.getString(s, null);
    }

    public String getSharedPreferenceOrderQueueDrink(String s){
        return sharedPreferencesOrderQueueDrink.getString(s, null);
    }

    public void clearSharedPreferenceOrderQueueFood(){
        editorOrderQueueFood.clear();
        editorOrderQueueFood.commit();
    }

    public void clearSharedPreferenceOrderQueueDrink(){
        editorOrderQueueDrink.clear();
        editorOrderQueueDrink.commit();
    }

    public static String getSharedPrefIP(){

        Log.d(StringConfig.LOG_TAG, "getIP from sharedpref : " + sharedPreferencesAdmin.getString("ipaddress", null));

        return sharedPreferencesAdmin.getString("ipaddress", null);
    }

    public static void setSharedPrefIP(String ipaddress){
        editorAdmin.putString("ipaddress",ipaddress);
        editorAdmin.apply();
    }

}
