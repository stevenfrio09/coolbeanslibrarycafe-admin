package com.steven.frio.coolbeanslibrarycafeadmin.recyclerparsers;

/**
 * Created by steven on 10/1/17.
 */

public class RParserDrinks {

    private Integer orderId;
    private Integer orderDrinkId;
    private Integer customerId;
    private Integer orderQuantityId;
    private Integer orderDrinkCategoryId;
    private Integer orderDrinkServingId;
    private boolean orderIsServed;
    private boolean orderIsPaid;
    private boolean orderIsCancelled;
    private String orderIsRedeemedWithPoints;
    private boolean orderIsDiscounted;
    private String orderDiscount;

    public RParserDrinks(Integer orderId, Integer orderDrinkId, Integer customerId, Integer orderQuantityId, Integer orderDrinkCategoryId, Integer orderDrinkServingId, Boolean orderIsServed, Boolean orderIsPaid, Boolean orderIsCancelled, String orderIsRedeemedWithPoints, boolean orderIsDiscounted, String orderDiscount){
        this.orderId = orderId;
        this.orderDrinkId = orderDrinkId;
        this.customerId = customerId;
        this.orderQuantityId = orderQuantityId;
        this.orderDrinkCategoryId = orderDrinkCategoryId;
        this.orderDrinkServingId = orderDrinkServingId;
        this.orderIsServed = orderIsServed;
        this.orderIsPaid = orderIsPaid;
        this.orderIsCancelled = orderIsCancelled;
        this.orderIsRedeemedWithPoints = orderIsRedeemedWithPoints;
        this.orderIsDiscounted = orderIsDiscounted;
        this.orderDiscount = orderDiscount;
    }

    public Integer getOrderId() {
        return orderId;
    }

    public void setOrderId(Integer orderId) {
        this.orderId = orderId;
    }

    public Integer getOrderDrinkId() {
        return orderDrinkId;
    }

    public void setOrderDrinkId(Integer orderDrinkId) {
        this.orderDrinkId = orderDrinkId;
    }

    public Integer getCustomerId() {
        return customerId;
    }

    public void setCustomerId(Integer customerId) {
        this.customerId = customerId;
    }

    public Integer getOrderQuantityId() {
        return orderQuantityId;
    }

    public void setOrderQuantityId(Integer orderQuantityId) {
        this.orderQuantityId = orderQuantityId;
    }

    public Integer getOrderDrinkCategoryId() {
        return orderDrinkCategoryId;
    }

    public void setOrderDrinkCategoryId(Integer orderDrinkCategoryId) {
        this.orderDrinkCategoryId = orderDrinkCategoryId;
    }

    public Integer getOrderDrinkServingId() {
        return orderDrinkServingId;
    }

    public void setOrderDrinkServingId(Integer orderDrinkServingId) {
        this.orderDrinkServingId = orderDrinkServingId;
    }

    public boolean isOrderIsServed() {
        return orderIsServed;
    }

    public void setOrderIsServed(boolean orderIsServed) {
        this.orderIsServed = orderIsServed;
    }

    public boolean isOrderIsPaid() {
        return orderIsPaid;
    }

    public void setOrderIsPaid(boolean orderIsPaid) {
        this.orderIsPaid = orderIsPaid;
    }

    public boolean isOrderIsCancelled() {
        return orderIsCancelled;
    }

    public void setOrderIsCancelled(boolean orderIsCancelled) {
        this.orderIsCancelled = orderIsCancelled;
    }

    public String getOrderIsRedeemedWithPoints() {
        return orderIsRedeemedWithPoints;
    }

    public void setOrderIsRedeemedWithPoints(String orderIsRedeemedWithPoints) {
        this.orderIsRedeemedWithPoints = orderIsRedeemedWithPoints;
    }

    public boolean isOrderIsDiscounted() {
        return orderIsDiscounted;
    }

    public void setOrderIsDiscounted(boolean orderIsDiscounted) {
        this.orderIsDiscounted = orderIsDiscounted;
    }

    public String getOrderDiscount() {
        return orderDiscount;
    }

    public void setOrderDiscount(String orderDiscount) {
        this.orderDiscount = orderDiscount;
    }
}
