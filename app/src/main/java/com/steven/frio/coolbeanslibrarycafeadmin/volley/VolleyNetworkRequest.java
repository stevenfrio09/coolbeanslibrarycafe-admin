package com.steven.frio.coolbeanslibrarycafeadmin.volley;

import android.support.design.widget.CoordinatorLayout;
import android.support.design.widget.Snackbar;
import android.support.v4.widget.SwipeRefreshLayout;
import android.widget.LinearLayout;

import com.android.volley.NetworkResponse;
import com.android.volley.NoConnectionError;
import com.android.volley.ServerError;
import com.android.volley.TimeoutError;
import com.android.volley.VolleyError;

/**
 * Created by steven on 10/1/17.
 */

public class VolleyNetworkRequest {

    public void volleyErrorLogger(VolleyError error, CoordinatorLayout cl_main) {
        NetworkResponse networkResponse = error.networkResponse;

        /*if (networkResponse != null && networkResponse.statusCode == 400) {
            Snackbar snackbar = Snackbar.make(cl_main, "Check your credentials.", Snackbar.LENGTH_SHORT);
            snackbar.show();
        }*/

        if (networkResponse != null && networkResponse.statusCode == 404) {
            Snackbar snackbar = Snackbar.make(cl_main, "Error 404. Server not found", Snackbar.LENGTH_SHORT);
            snackbar.show();
        }

        if (error instanceof TimeoutError) {
            Snackbar snackbar = Snackbar.make(cl_main, "Request Timed Out", Snackbar.LENGTH_SHORT);
            snackbar.show();
        }

        if (error instanceof NoConnectionError) {
            Snackbar snackbar = Snackbar.make(cl_main, "No Connection", Snackbar.LENGTH_SHORT);
            snackbar.show();
        }

        if (error instanceof ServerError) {
            Snackbar snackbar = Snackbar.make(cl_main, "Server Error", Snackbar.LENGTH_SHORT);
            snackbar.show();
        }
    }
}
