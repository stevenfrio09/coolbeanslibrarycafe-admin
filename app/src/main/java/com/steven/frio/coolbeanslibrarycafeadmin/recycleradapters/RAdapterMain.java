package com.steven.frio.coolbeanslibrarycafeadmin.recycleradapters;

import android.content.Context;
import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.TextView;

import com.steven.frio.coolbeanslibrarycafeadmin.R;
import com.steven.frio.coolbeanslibrarycafeadmin.R2;
import com.steven.frio.coolbeanslibrarycafeadmin.recyclerparsers.RParserMain;
import java.util.List;

import butterknife.BindView;
import butterknife.ButterKnife;

/**
 * Created by steven on 9/12/17.
 */

public class RAdapterMain extends RecyclerView.Adapter<RAdapterMain.ViewHolder> {

    /*public interface RecyclerViewToMainInterface{
        void displayQueuedOrder(ViewHolder holder, int position);

    }*/

    List<RParserMain> rParserMainList;
    Context context;

    public RAdapterMain(Context context, List<RParserMain> rParserMainList){
        this.context = context;
        this.rParserMainList = rParserMainList;

    }

    @Override
    public ViewHolder onCreateViewHolder(ViewGroup parent, int viewType) {
        View view = LayoutInflater.from(context).inflate(R.layout.cardview_holder_main, parent, false);
        return new ViewHolder(view);
    }

    @Override
    public void onBindViewHolder(final ViewHolder holder, final int position) {
        RParserMain rParserMain = rParserMainList.get(position);

        holder.tv_cardMain_id.setText(rParserMain.getOrderId().toString());
        holder.tv_cardMain_customerName.setText(String.valueOf(rParserMain.getCustId()));
        holder.tv_cardMain_orderName.setText(rParserMain.getOrderName());
        holder.tv_cardMain_orderCategory.setText(rParserMain.getOrderCategory());
        holder.tv_cardMain_orderSize.setText(rParserMain.getOrderSize());
        holder.tv_cardMain_orderQuantity.setText(String.valueOf(rParserMain.getQuantity()));
    }

    @Override
    public int getItemCount() {
        return rParserMainList.size();
    }

    public class ViewHolder extends RecyclerView.ViewHolder {

        @BindView(R2.id.tv_cardMain_id)
        TextView tv_cardMain_id;

        @BindView(R2.id.tv_cardMain_customerName)
        TextView tv_cardMain_customerName;

        @BindView(R2.id.tv_cardMain_orderName)
        TextView tv_cardMain_orderName;

        @BindView(R2.id.tv_cardMain_orderCategory)
        TextView tv_cardMain_orderCategory;

        @BindView(R2.id.tv_cardMain_orderSize)
        TextView tv_cardMain_orderSize;

        @BindView(R2.id.tv_cardMain_orderQuantity)
        TextView tv_cardMain_orderQuantity;

        public ViewHolder(View itemView) {
            super(itemView);

            ButterKnife.bind(this, itemView);
            ButterKnife.setDebug(true);
        }
    }
}
