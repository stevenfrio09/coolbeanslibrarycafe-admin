package com.steven.frio.coolbeanslibrarycafeadmin.recycleradapters;

import android.content.Context;
import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.TextView;

import com.steven.frio.coolbeanslibrarycafeadmin.R;
import com.steven.frio.coolbeanslibrarycafeadmin.R2;
import com.steven.frio.coolbeanslibrarycafeadmin.recyclerparsers.RParserFoods;
import com.steven.frio.coolbeanslibrarycafeadmin.sharedpreferences.SharedPreferencesManager;
import com.steven.frio.coolbeanslibrarycafeadmin.sqlite.SQLiteCBLCAdapter;

import java.util.List;

import butterknife.BindInt;
import butterknife.BindView;
import butterknife.ButterKnife;

/**
 * Created by steven on 10/1/17.
 */

public class RAdapterFoods extends RecyclerView.Adapter<RAdapterFoods.ViewHolder> {


    Context context;
    List<RParserFoods> rParserFoodsList;
    SharedPreferencesManager sharedPreferencesManager;

    public RAdapterFoods(Context context, List<RParserFoods> rParserFoodsList){
        this.context = context;
        this.rParserFoodsList = rParserFoodsList;
    }

    @Override
    public ViewHolder onCreateViewHolder(ViewGroup parent, int viewType) {
        View view = LayoutInflater.from(context).inflate(R.layout.cardview_recyclerholder_foods, parent, false);
        return new ViewHolder(view);
    }

    @Override
    public void onBindViewHolder(ViewHolder holder, int position) {
        RParserFoods rParserFoods = rParserFoodsList.get(position);

        sharedPreferencesManager = new SharedPreferencesManager(context);
        String customerName = sharedPreferencesManager.getSharedPreferencesListUser(String.valueOf(rParserFoods.getCustomerId()));
        String orderName = sharedPreferencesManager.getSharedPreferencesFoodMain(String.valueOf(rParserFoods.getOrderFoodId()));
        String orderCategory = sharedPreferencesManager.getSharedPreferencesFoodCategory(String.valueOf(rParserFoods.getOrderFoodCategoryId()));
        String orderServing = sharedPreferencesManager.getSharedPreferencesFoodServing(String.valueOf(rParserFoods.getOrderFoodServingId()));

        holder.tv_recyclerholderfoods_orderName.setText(orderName);
        holder.tv_recyclerholderfoods_orderQuantity.setText(String.valueOf(rParserFoods.getOrderQuantityId()));
        holder.tv_recyclerholderfoods_orderSize.setText(orderServing);

    }

    @Override
    public int getItemCount() {
        return rParserFoodsList.size();
    }

    public class ViewHolder extends RecyclerView.ViewHolder {

        @BindView(R2.id.tv_recyclerholderfoods_orderName)
        TextView tv_recyclerholderfoods_orderName;

        @BindView(R2.id.tv_recyclerholderfoods_orderQuantity)
        TextView tv_recyclerholderfoods_orderQuantity;

        @BindView(R2.id.tv_recyclerholderfoods_orderSize)
        TextView tv_recyclerholderfoods_orderSize;

        public ViewHolder(View itemView) {
            super(itemView);

            ButterKnife.setDebug(true);
            ButterKnife.bind(this, itemView);
        }
    }
}
