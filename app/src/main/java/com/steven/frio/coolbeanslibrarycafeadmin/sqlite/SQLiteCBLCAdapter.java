package com.steven.frio.coolbeanslibrarycafeadmin.sqlite;

import android.content.ContentValues;
import android.content.Context;
import android.database.Cursor;
import android.database.sqlite.SQLiteDatabase;
import android.database.sqlite.SQLiteOpenHelper;
import android.util.Log;

import com.steven.frio.coolbeanslibrarycafeadmin.configs.StringConfig;

/**
 * Created by steven on 10/2/17.
 */

public class SQLiteCBLCAdapter {

    SQLiteCBLC sqLiteCBLC;
    SQLiteDatabase sqLiteDatabase;

    public SQLiteCBLCAdapter(Context context) {
        sqLiteCBLC = new SQLiteCBLC(context);
        sqLiteDatabase = sqLiteCBLC.getWritableDatabase();
    }

    public long insertDataForFoods(int mainFoodId, String mainFoodName, int categoryFoodId, String categoryFoodName, int servingFoodId, String servingFoodName) {

        ContentValues contentValues = new ContentValues();
        contentValues.put(SQLiteCBLC.COL_FOOD_MAIN_ID, mainFoodId);
        contentValues.put(SQLiteCBLC.COL_FOOD_MAIN_NAME, mainFoodName);
        contentValues.put(SQLiteCBLC.COL_FOOD_CATEGORY_ID, categoryFoodId);
        contentValues.put(SQLiteCBLC.COL_FOOD_CATEGORY_NAME, categoryFoodName);
        contentValues.put(SQLiteCBLC.COL_FOOD_SERVING_ID, servingFoodId);
        contentValues.put(SQLiteCBLC.COL_FOOD_SERVING_NAME, servingFoodName);

        long id = sqLiteDatabase.insert(SQLiteCBLC.TABLE_NAME_FOOD, null, contentValues);
        return id;
    }

    public long insertDataForDrinks(int mainDrinkId, String mainDrinkName, int categoryDrinkId, String categoryDrinkName, int servingDrinkId, String servingDrinkName) {

        ContentValues contentValues = new ContentValues();
        contentValues.put(SQLiteCBLC.COL_DRINK_MAIN_ID, mainDrinkId);
        contentValues.put(SQLiteCBLC.COL_DRINK_MAIN_NAME, mainDrinkName);
        contentValues.put(SQLiteCBLC.COL_DRINK_CATEGORY_ID, categoryDrinkId);
        contentValues.put(SQLiteCBLC.COL_DRINK_CATEGORY_NAME, categoryDrinkName);
        contentValues.put(SQLiteCBLC.COL_DRINK_SERVING_ID, servingDrinkId);
        contentValues.put(SQLiteCBLC.COL_DRINK_SERVING_NAME, servingDrinkName);

        long id = sqLiteDatabase.insert(SQLiteCBLC.TABLE_NAME_DRINK, null, contentValues);
        return id;
    }

    public String selectFoodName(String foodId) {
        String selectQuery = "SELECT " + SQLiteCBLC.COL_FOOD_MAIN_NAME + " FROM " + SQLiteCBLC.TABLE_NAME_FOOD + " WHERE " + SQLiteCBLC.COL_FOOD_ID + " = '" + foodId + "'";

        String foodName = "";
        Cursor cursor = sqLiteDatabase.rawQuery(selectQuery, null);
        while (cursor.moveToFirst()){
            foodName = cursor.getString(0);
            Log.d(StringConfig.LOG_TAG, "selectQuery : " + foodName);
        }

        cursor.close();
        return foodName;
    }

    public String selectDrinkName() {
        String drinkName = "";


        return drinkName;
    }


    public static class SQLiteCBLC extends SQLiteOpenHelper {

        public static final String DATABASE_NAME = "cblc_product_list.db";


        public static final String TABLE_NAME_FOOD = "cblc_table_food";

        public static final String COL_FOOD_ID = "ID";

        public static final String COL_FOOD_MAIN_ID = "FOOD_MAIN_ID";
        public static final String COL_FOOD_MAIN_NAME = "FOOD_MAIN_NAME";

        public static final String COL_FOOD_CATEGORY_ID = "FOOD_CATEGORY_ID";
        public static final String COL_FOOD_CATEGORY_NAME = "FOOD_CATEGORY_NAME";

        public static final String COL_FOOD_SERVING_ID = "FOOD_SERVING_ID";
        public static final String COL_FOOD_SERVING_NAME = "FOOD_SERVING_NAME";


        public static final String TABLE_NAME_DRINK = "cblc_table_drink";

        public static final String COL_DRINK_ID = "ID";

        public static final String COL_DRINK_MAIN_ID = "DRINK_MAIN_ID";
        public static final String COL_DRINK_MAIN_NAME = "DRINK_MAIN_NAME";

        public static final String COL_DRINK_CATEGORY_ID = "DRINK_CATEGORY_ID";
        public static final String COL_DRINK_CATEGORY_NAME = "DRINK_CATEGORY_NAME";

        public static final String COL_DRINK_SERVING_ID = "DRINK_SERVING_ID";
        public static final String COL_DRINK_SERVING_NAME = "DRINK_SERVING_NAME";

        private static final int DATABASE_VERSION = 1;

        private static final String createFoodsListTable = "CREATE TABLE " + TABLE_NAME_FOOD + "(" + COL_FOOD_ID + " INTEGER PRIMARY KEY AUTOINCREMENT, " + COL_FOOD_MAIN_ID + ", " + COL_FOOD_MAIN_NAME + ", " + COL_FOOD_CATEGORY_ID + ", " + COL_FOOD_CATEGORY_NAME + ", " + COL_FOOD_SERVING_ID + ", " + COL_FOOD_SERVING_NAME + ")";
        private static final String createDrinksListTable = "CREATE TABLE " + TABLE_NAME_DRINK + "(" + COL_DRINK_MAIN_ID + ", " + COL_DRINK_MAIN_NAME + ", " + COL_DRINK_CATEGORY_ID + ", " + COL_DRINK_CATEGORY_NAME + ", " + COL_DRINK_SERVING_ID + ", " + COL_DRINK_SERVING_NAME + ")";
        private static final String dropFoodsList = "DROP TABLE IF EXISTS " + TABLE_NAME_FOOD + "";
        private static final String dropDrinksList = "DROP TABLE IF EXISTS " + TABLE_NAME_DRINK + "";

        public SQLiteCBLC(Context context) {
            super(context, DATABASE_NAME, null, DATABASE_VERSION);
            SQLiteDatabase sqLiteDatabase = this.getWritableDatabase();
        }

        @Override
        public void onCreate(SQLiteDatabase sqLiteDatabase) {
            sqLiteDatabase.execSQL(createFoodsListTable);
            sqLiteDatabase.execSQL(createDrinksListTable);
        }

        @Override
        public void onUpgrade(SQLiteDatabase sqLiteDatabase, int i, int i1) {
            sqLiteDatabase.execSQL(dropFoodsList);
            sqLiteDatabase.execSQL(dropDrinksList);
        }
    }
}
