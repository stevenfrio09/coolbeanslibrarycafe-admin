package com.steven.frio.coolbeanslibrarycafeadmin.recycleradapters;

import android.content.Context;
import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.TextView;

import com.steven.frio.coolbeanslibrarycafeadmin.R;
import com.steven.frio.coolbeanslibrarycafeadmin.R2;
import com.steven.frio.coolbeanslibrarycafeadmin.recyclerparsers.RParserDrinks;
import com.steven.frio.coolbeanslibrarycafeadmin.sharedpreferences.SharedPreferencesManager;

import java.util.List;

import butterknife.BindView;
import butterknife.ButterKnife;

/**
 * Created by steven on 10/1/17.
 */

public class RAdapterDrinks extends RecyclerView.Adapter<RAdapterDrinks.ViewHolder> {

    Context context;
    List<RParserDrinks> rParserDrinksList;
    SharedPreferencesManager sharedPreferencesManager;

    public RAdapterDrinks(Context context, List<RParserDrinks> rParserDrinksList){
        this.context = context;
        this.rParserDrinksList = rParserDrinksList;
    }

    @Override
    public ViewHolder onCreateViewHolder(ViewGroup parent, int viewType) {
        View view = LayoutInflater.from(context).inflate(R.layout.cardview_recyclerholder_drinks, parent, false);
        return new ViewHolder(view);
    }

    @Override
    public void onBindViewHolder(ViewHolder holder, int position) {
        RParserDrinks rParserDrinks = rParserDrinksList.get(position);

        sharedPreferencesManager = new SharedPreferencesManager(context);

        String customerName = sharedPreferencesManager.getSharedPreferencesListUser(String.valueOf(rParserDrinks.getCustomerId()));
        String orderName = sharedPreferencesManager.getSharedPreferencesDrinkMain(String.valueOf(rParserDrinks.getOrderDrinkId()));
        String orderCategory = sharedPreferencesManager.getSharedPreferencesDrinkCategory(String.valueOf(rParserDrinks.getOrderDrinkCategoryId()));
        String orderServing = sharedPreferencesManager.getSharedPreferencesDrinkServing(String.valueOf(rParserDrinks.getOrderDrinkServingId()));

        holder.tv_recyclerholderdrinks_orderName.setText(orderName);
        holder.tv_recyclerholderdrinks_orderQuantity.setText(String.valueOf(rParserDrinks.getOrderQuantityId()));
        holder.tv_recyclerholderdrinks_orderSize.setText(orderServing);

    }

    @Override
    public int getItemCount() {
        return rParserDrinksList.size();
    }

    public class ViewHolder extends RecyclerView.ViewHolder {

        @BindView(R2.id.tv_recyclerholderdrinks_orderName)
        TextView tv_recyclerholderdrinks_orderName;

        @BindView(R2.id.tv_recyclerholderdrinks_orderQuantity)
        TextView tv_recyclerholderdrinks_orderQuantity;

        @BindView(R2.id.tv_recyclerholderdrinks_orderSize)
        TextView tv_recyclerholderdrinks_orderSize;

        public ViewHolder(View itemView) {
            super(itemView);

            ButterKnife.setDebug(true);
            ButterKnife.bind(this, itemView);
        }
    }
}
