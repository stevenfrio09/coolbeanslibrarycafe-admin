package com.steven.frio.coolbeanslibrarycafeadmin;

import android.content.DialogInterface;
import android.content.Intent;
import android.os.Bundle;
import android.support.design.widget.CoordinatorLayout;
import android.support.design.widget.Snackbar;
import android.support.v4.widget.SwipeRefreshLayout;
import android.support.v7.app.AlertDialog;
import android.support.v7.app.AppCompatActivity;
import android.support.v7.widget.CardView;
import android.support.v7.widget.DividerItemDecoration;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.util.Log;
import android.view.View;
import android.widget.Button;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.RelativeLayout;
import android.widget.TextView;

import com.android.volley.Request;
import com.android.volley.RequestQueue;
import com.android.volley.Response;
import com.android.volley.VolleyError;
import com.android.volley.toolbox.JsonObjectRequest;
import com.android.volley.toolbox.StringRequest;
import com.android.volley.toolbox.Volley;
import com.steven.frio.coolbeanslibrarycafeadmin.configs.StringConfig;
import com.steven.frio.coolbeanslibrarycafeadmin.recycleradapters.RAdapterDrinks;
import com.steven.frio.coolbeanslibrarycafeadmin.recycleradapters.RAdapterFoods;
import com.steven.frio.coolbeanslibrarycafeadmin.recyclerparsers.RParserDrinks;
import com.steven.frio.coolbeanslibrarycafeadmin.recyclerparsers.RParserFoods;
import com.steven.frio.coolbeanslibrarycafeadmin.sharedpreferences.SharedPreferencesManager;
import com.steven.frio.coolbeanslibrarycafeadmin.volley.CustomRequest;
import com.steven.frio.coolbeanslibrarycafeadmin.volley.VolleyNetworkRequest;

import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

import butterknife.BindView;
import butterknife.ButterKnife;

public class MainActivity extends AppCompatActivity {

    @BindView(R2.id.cl_main)
    CoordinatorLayout cl_main;

    @BindView(R2.id.srl_foods)
    SwipeRefreshLayout srl_foods;

    @BindView(R2.id.srl_drinks)
    SwipeRefreshLayout srl_drinks;

    @BindView(R2.id.rv_foods)
    RecyclerView rv_foods;

    @BindView(R2.id.rv_drinks)
    RecyclerView rv_drinks;


    @BindView(R2.id.btn_servedFood)
    Button btn_servedFood;

    @BindView(R2.id.btn_servedDrink)
    Button btn_servedDrink;

    @BindView(R2.id.btn_cancelFood)
    Button btn_cancelFood;

    @BindView(R2.id.btn_cancelDrink)
    Button btn_cancelDrink;


    //cardview_holder_foods
    @BindView(R2.id.cardview_holder_foods)
    LinearLayout cardview_holder_foods;

    @BindView(R2.id.contentholder_order_foods)
    RelativeLayout contentholder_order_foods;


    @BindView(R2.id.tv_cardFood_orderId)
    TextView tv_cardFood_orderId;

    @BindView(R2.id.tv_cardFood_customerName)
    TextView tv_cardFood_customerName;

    @BindView(R2.id.tv_cardFood_orderName)
    TextView tv_cardFood_orderName;

    @BindView(R2.id.tv_cardFood_orderCategory)
    TextView tv_cardFood_orderCategory;

    @BindView(R2.id.tv_cardFood_orderSize)
    TextView tv_cardFood_orderSize;

    @BindView(R2.id.tv_cardFood_orderQuantity)
    TextView tv_cardFood_orderQuantity;

    @BindView(R2.id.tv_cardFood_orderIsDiscounted)
    TextView tv_cardFood_orderIsDiscounted;

    @BindView(R2.id.tv_cardFood_orderDiscount)
    TextView tv_cardFood_orderDiscount;

    @BindView(R2.id.tv_cardFood_orderFoodId)
    TextView tv_cardFood_orderFoodId;

    //cardview_holder_drinks
    @BindView(R2.id.cardview_holder_drinks)
    LinearLayout cardview_holder_drinks;

    @BindView(R2.id.contentholder_order_drinks)
    RelativeLayout contentholder_order_drinks;


    @BindView(R2.id.tv_cardDrink_orderId)
    TextView tv_cardDrink_orderId;

    @BindView(R2.id.tv_cardDrink_customerName)
    TextView tv_cardDrink_customerName;

    @BindView(R2.id.tv_cardDrink_orderName)
    TextView tv_cardDrink_orderName;

    @BindView(R2.id.tv_cardDrink_orderCategory)
    TextView tv_cardDrink_orderCategory;

    @BindView(R2.id.tv_cardDrink_orderSize)
    TextView tv_cardDrink_orderSize;

    @BindView(R2.id.tv_cardDrink_orderQuantity)
    TextView tv_cardDrink_orderQuantity;

    @BindView(R2.id.tv_cardDrink_isPaidWithPoints)
    TextView tv_cardDrink_isPaidWithPoints;

    @BindView(R2.id.tv_cardDrink_orderIsDiscounted)
    TextView tv_cardDrink_orderIsDiscounted;

    @BindView(R2.id.tv_cardDrink_orderDiscount)
    TextView tv_cardDrink_orderDiscount;


    @BindView(R2.id.tv_cardDrink_orderDrinkId)
    TextView tv_cardDrink_orderDrinkId;

    //Content holder
    @BindView(R2.id.tv_confirmServedFood_orderName)
    TextView tv_confirmServedFood_orderName;

    @BindView(R2.id.tv_confirmServedDrink_orderName)
    TextView tv_confirmServedDrink_orderName;

    @BindView(R2.id.tv_confirmServedFood_orderQuantity)
    TextView tv_confirmServedFood_orderQuantity;

    @BindView(R2.id.tv_confirmServedDrink_orderQuantity)
    TextView tv_confirmServedDrink_orderQuantity;

    @BindView(R2.id.iv_increaseServedFoodQuantity)
    ImageView iv_increaseServedFoodQuantity;

    @BindView(R2.id.iv_reduceServedFoodQuantity)
    ImageView iv_reduceServedFoodQuantity;

    @BindView(R2.id.iv_increaseServedDrinkQuantity)
    ImageView iv_increaseServedDrinkQuantity;

    @BindView(R2.id.iv_reduceServedDrinkQuantity)
    ImageView iv_reduceServedDrinkQuantity;


    @BindView(R2.id.rl_card_foods_tv)
    CardView rl_card_foods_tv;

    @BindView(R2.id.rl_card_drinks_tv)
            CardView rl_card_drinks_tv;

    // SNACKBAR ON BACKPRESSED
    Snackbar snackbarBackPressed;

    // init recyclerview
    RAdapterFoods rAdapterFoods;
    RAdapterDrinks rAdapterDrinks;
    List<RParserFoods> rParserFoodsList;
    List<RParserDrinks> rParserDrinksList;
    RecyclerView.LayoutManager layoutManagerFoods, layoutManagerDrinks;

    //init network
    RequestQueue requestQueue;

    //init sharedpref

    SharedPreferencesManager sharedPreferencesManager;

    //strings
    String requestFood = "Food";
    String requestDrink = "Drink";

    VolleyNetworkRequest volleyNetworkRequest;

    /*SQLiteCBLCAdapter sqLiteCBLCAdapter;
    SQLiteCBLCAdapter.SQLiteCBLC sqLiteCBLC;
    SQLiteDatabase sqLiteDatabase;*/

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);

        ButterKnife.bind(this);
        ButterKnife.setDebug(true);

        rParserFoodsList = new ArrayList<>();
        rParserDrinksList = new ArrayList<>();


        sharedPreferencesManager = new SharedPreferencesManager(MainActivity.this);

        snackbarBackPressed = Snackbar.make(cl_main, "Press back again to exit.", Snackbar.LENGTH_SHORT);

        volleyNetworkRequest = new VolleyNetworkRequest();

        //sqLiteCBLCAdapter = new SQLiteCBLCAdapter(MainActivity.this);

        //clears queue
        sharedPreferencesManager.clearSharedPreferenceOrderQueueFood();
        sharedPreferencesManager.clearSharedPreferenceOrderQueueDrink();

        getCustomerIdAndNameVolley();
        getFoodListVolley();
        getDrinkListVolley();

        refreshFoods();
        refreshDrinks();


        srl_foods.setOnRefreshListener(new SwipeRefreshLayout.OnRefreshListener() {
            @Override
            public void onRefresh() {
                refreshFoods();
            }
        });

        srl_drinks.setOnRefreshListener(new SwipeRefreshLayout.OnRefreshListener() {
            @Override
            public void onRefresh() {
                refreshDrinks();
            }
        });

        onClickListeners();

        onlongClickListeners();

        onHeaderLongClick();

    }

    private void onClickListeners() {
        iv_increaseServedFoodQuantity.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                String getOrderName = tv_confirmServedFood_orderName.getText().toString();
                int getOrderQuantity = Integer.parseInt(tv_cardFood_orderQuantity.getText().toString());
                int getSharedPrefQuantity = Integer.parseInt(sharedPreferencesManager.getSharedPreferenceOrderQueueFood(getOrderName));
                if (getOrderQuantity != getSharedPrefQuantity) {

                    getSharedPrefQuantity++;
                    //int sharedPrefQuantityValue = Integer.parseInt(sharedPreferencesManager.getSharedPreferenceOrderQueueFood(orderName)) - 1;

                    sharedPreferencesManager.setEditorOrderQueueFood(getOrderName, String.valueOf(getSharedPrefQuantity));
                    tv_confirmServedFood_orderQuantity.setText(sharedPreferencesManager.getSharedPreferenceOrderQueueFood(getOrderName));

                }
            }
        });

        iv_increaseServedDrinkQuantity.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                String getOrderName = tv_confirmServedDrink_orderName.getText().toString();
                int getOrderQuantity = Integer.parseInt(tv_cardDrink_orderQuantity.getText().toString());
                int getSharedPrefQuantity = Integer.parseInt(sharedPreferencesManager.getSharedPreferenceOrderQueueDrink(getOrderName));


                if (getOrderQuantity != getSharedPrefQuantity) {

                    getSharedPrefQuantity++;
                    //int sharedPrefQuantityValue = Integer.parseInt(sharedPreferencesManager.getSharedPreferenceOrderQueueFood(orderName)) - 1;

                    sharedPreferencesManager.setEditorOrderQueueDrink(getOrderName, String.valueOf(getSharedPrefQuantity));
                    tv_confirmServedDrink_orderQuantity.setText(sharedPreferencesManager.getSharedPreferenceOrderQueueDrink(getOrderName));

                }
            }
        });

        iv_reduceServedFoodQuantity.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                String strServeOrderUrl = StringConfig.BASE_URL + StringConfig.SERVED_ORDER;
                String getOrderName = tv_confirmServedFood_orderName.getText().toString();

                String getOrderIsDiscounted = tv_cardFood_orderIsDiscounted.getText().toString();
                String getOrderDiscount = tv_cardFood_orderDiscount.getText().toString();
                try {
                    int getOrderQuantity = Integer.parseInt(sharedPreferencesManager.getSharedPreferenceOrderQueueFood(getOrderName));
                    String type = "food";

                    if (getOrderQuantity > 1) {
                        getOrderQuantity--;
                        //int sharedPrefQuantityValue = Integer.parseInt(sharedPreferencesManager.getSharedPreferenceOrderQueueFood(orderName)) - 1;

                        sharedPreferencesManager.setEditorOrderQueueFood(getOrderName, String.valueOf(getOrderQuantity));
                        tv_confirmServedFood_orderQuantity.setText(sharedPreferencesManager.getSharedPreferenceOrderQueueFood(getOrderName));

                    } else if (getOrderQuantity == 1) {
                        getOrderIdAndOrder(strServeOrderUrl, type, getOrderQuantity, getOrderIsDiscounted, getOrderDiscount);
                    }

                } catch (NumberFormatException e) {

                }
            }
        });

        iv_reduceServedDrinkQuantity.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                String strServeOrderUrl = StringConfig.BASE_URL + StringConfig.SERVED_ORDER;
                String getOrderName = tv_confirmServedDrink_orderName.getText().toString();

                String getOrderIsDiscounted = tv_cardFood_orderIsDiscounted.getText().toString();
                String getOrderDiscount = tv_cardFood_orderDiscount.getText().toString();

                try {
                    int getOrderQuantity = Integer.parseInt(sharedPreferencesManager.getSharedPreferenceOrderQueueDrink(getOrderName));
                    String type = "drink";

                    if (getOrderQuantity > 1) {
                        getOrderQuantity--;
                        //int sharedPrefQuantityValue = Integer.parseInt(sharedPreferencesManager.getSharedPreferenceOrderQueueFood(orderName)) - 1;

                        sharedPreferencesManager.setEditorOrderQueueDrink(getOrderName, String.valueOf(getOrderQuantity));
                        tv_confirmServedDrink_orderQuantity.setText(sharedPreferencesManager.getSharedPreferenceOrderQueueDrink(getOrderName));

                    } else if (getOrderQuantity == 1) {
                        getOrderIdAndOrder(strServeOrderUrl, type, getOrderQuantity, getOrderIsDiscounted, getOrderDiscount);
                    }

                } catch (NumberFormatException e) {

                }
            }
        });
    }

    private void onlongClickListeners() {
        iv_reduceServedDrinkQuantity.setOnLongClickListener(new View.OnLongClickListener() {
            @Override
            public boolean onLongClick(View view) {
                String strServeOrderUrl = StringConfig.BASE_URL + StringConfig.SERVED_ORDER;
                String getOrderName = tv_confirmServedDrink_orderName.getText().toString();

                String getOrderIsDiscounted = tv_cardFood_orderIsDiscounted.getText().toString();
                String getOrderDiscount = tv_cardFood_orderDiscount.getText().toString();

                try {
                    int getOrderQuantity = Integer.parseInt(sharedPreferencesManager.getSharedPreferenceOrderQueueDrink(getOrderName));
                    String type = "drink";

                    if (getOrderQuantity > 1) {
                        //int sharedPrefQuantityValue = Integer.parseInt(sharedPreferencesManager.getSharedPreferenceOrderQueueFood(orderName)) - 1;

                        sharedPreferencesManager.setEditorOrderQueueDrink(getOrderName, String.valueOf(1));
                        tv_confirmServedDrink_orderQuantity.setText(sharedPreferencesManager.getSharedPreferenceOrderQueueDrink(getOrderName));

                    } /*else if (getOrderQuantity == 1) {
                        getOrderIdAndOrder(strServeOrderUrl, type, getOrderQuantity, getOrderIsDiscounted, getOrderDiscount);
                    }*/

                } catch (NumberFormatException e) {

                }
                return true;
            }
        });


        iv_reduceServedFoodQuantity.setOnLongClickListener(new View.OnLongClickListener() {
            @Override
            public boolean onLongClick(View view) {
                String strServeOrderUrl = StringConfig.BASE_URL + StringConfig.SERVED_ORDER;
                String getOrderName = tv_confirmServedFood_orderName.getText().toString();

                String getOrderIsDiscounted = tv_cardFood_orderIsDiscounted.getText().toString();
                String getOrderDiscount = tv_cardFood_orderDiscount.getText().toString();
                try {
                    int getOrderQuantity = Integer.parseInt(sharedPreferencesManager.getSharedPreferenceOrderQueueFood(getOrderName));
                    String type = "food";

                    if (getOrderQuantity > 1) {
                        //int sharedPrefQuantityValue = Integer.parseInt(sharedPreferencesManager.getSharedPreferenceOrderQueueFood(orderName)) - 1;

                        sharedPreferencesManager.setEditorOrderQueueFood(getOrderName, String.valueOf(1));
                        tv_confirmServedFood_orderQuantity.setText(sharedPreferencesManager.getSharedPreferenceOrderQueueFood(getOrderName));

                    } /*else if (getOrderQuantity == 1) {
                        getOrderIdAndOrder(strServeOrderUrl, type, getOrderQuantity, getOrderIsDiscounted, getOrderDiscount);
                    }*/

                } catch (NumberFormatException e) {

                }
                return true;
            }
        });


        iv_increaseServedFoodQuantity.setOnLongClickListener(new View.OnLongClickListener() {
            @Override
            public boolean onLongClick(View view) {

                String getOrderName = tv_confirmServedFood_orderName.getText().toString();
                int getOrderQuantity = Integer.parseInt(tv_cardFood_orderQuantity.getText().toString());
                int getSharedPrefQuantity = Integer.parseInt(sharedPreferencesManager.getSharedPreferenceOrderQueueFood(getOrderName));
                if (getOrderQuantity != getSharedPrefQuantity) {

                    //int sharedPrefQuantityValue = Integer.parseInt(sharedPreferencesManager.getSharedPreferenceOrderQueueFood(orderName)) - 1;

                    sharedPreferencesManager.setEditorOrderQueueFood(getOrderName, String.valueOf(getOrderQuantity));
                    tv_confirmServedFood_orderQuantity.setText(sharedPreferencesManager.getSharedPreferenceOrderQueueFood(getOrderName));

                }
                return true;
            }
        });

        iv_increaseServedDrinkQuantity.setOnLongClickListener(new View.OnLongClickListener() {
            @Override
            public boolean onLongClick(View view) {
                String getOrderName = tv_confirmServedDrink_orderName.getText().toString();
                int getOrderQuantity = Integer.parseInt(tv_cardDrink_orderQuantity.getText().toString());
                int getSharedPrefQuantity = Integer.parseInt(sharedPreferencesManager.getSharedPreferenceOrderQueueDrink(getOrderName));


                if (getOrderQuantity != getSharedPrefQuantity) {

                    //int sharedPrefQuantityValue = Integer.parseInt(sharedPreferencesManager.getSharedPreferenceOrderQueueFood(orderName)) - 1;

                    sharedPreferencesManager.setEditorOrderQueueDrink(getOrderName, String.valueOf(getOrderQuantity));
                    tv_confirmServedDrink_orderQuantity.setText(sharedPreferencesManager.getSharedPreferenceOrderQueueDrink(getOrderName));

                }
                return true;
            }
        });
    }

    private void onHeaderLongClick(){

        rl_card_foods_tv.setOnLongClickListener(new View.OnLongClickListener() {
            @Override
            public boolean onLongClick(View view) {

                Intent intent = new Intent(MainActivity.this, AdminActivity.class);
                startActivity(intent);

                return false;
            }
        });

        rl_card_drinks_tv.setOnLongClickListener(new View.OnLongClickListener() {
            @Override
            public boolean onLongClick(View view) {
                Intent intent = new Intent(MainActivity.this, AdminActivity.class);
                startActivity(intent);

                return false;
            }
        });
    }

    private void refreshDrinks() {
        srl_drinks.setRefreshing(true);
        initDrinkRecyclerView();
        srl_drinks.setRefreshing(false);
    }

    private void refreshFoods() {
        srl_foods.setRefreshing(true);
        initFoodRecyclerView();
        srl_foods.setRefreshing(false);
    }

    @Override
    public void onBackPressed() {

        if (!snackbarBackPressed.isShown()) {
            snackbarBackPressed.show();
        } else {
            super.onBackPressed();
        }
    }


    //get all registered customer id and customer name
    private void getCustomerIdAndNameVolley() {
        String getCustomerInfoUrl = StringConfig.BASE_URL + StringConfig.ROUTE_CUSTOMER_INFO;
        //String getCustomerInfoUrl = "http://192.168.43.91/valuesvalues.json";

        requestQueue = Volley.newRequestQueue(MainActivity.this);

        StringRequest stringRequest = new StringRequest(getCustomerInfoUrl, new Response.Listener<String>() {
            @Override
            public void onResponse(String response) {
                try {
                    JSONArray jsonArray = new JSONArray(response);
                    Log.d(StringConfig.LOG_TAG, "jsonArrayBlaBla : " + jsonArray);

                    for (int i = 0; i <= jsonArray.length(); i++) {
                        JSONObject jsonObject = jsonArray.getJSONObject(i);

                        String customerId = jsonObject.getString("id");
                        String customerName = jsonObject.getString("first_name");

                        sharedPreferencesManager.setEditorListUser(String.valueOf(customerId), customerName);
                    }

                } catch (JSONException e) {

                }
            }
        }, new Response.ErrorListener() {
            @Override
            public void onErrorResponse(VolleyError error) {
                volleyNetworkRequest.volleyErrorLogger(error, cl_main);
            }
        });
        requestQueue.add(stringRequest);

    }

    public void clearFoodTextViewContents() {

        tv_cardFood_orderId.setText("");
        tv_cardFood_customerName.setText("");
        tv_cardFood_orderName.setText("");
        tv_cardFood_orderCategory.setText("");
        tv_cardFood_orderSize.setText("");
        tv_cardFood_orderQuantity.setText("");

        tv_confirmServedFood_orderName.setText("");
        tv_confirmServedFood_orderQuantity.setText("");

    }

    public void clearDrinkTextViewContents() {

        tv_cardDrink_orderId.setText("");
        tv_cardDrink_customerName.setText("");
        tv_cardDrink_orderName.setText("");
        tv_cardDrink_orderCategory.setText("");
        tv_cardDrink_orderSize.setText("");
        tv_cardDrink_orderQuantity.setText("");

        tv_confirmServedDrink_orderName.setText("");
        tv_confirmServedDrink_orderQuantity.setText("");

    }

    //gets all product information and stores in database
    private void getFoodListVolley() {
        requestQueue = Volley.newRequestQueue(MainActivity.this);

        String strGetFoodListUrl = StringConfig.BASE_URL + StringConfig.ROUTE_SHOWALL_FOOD;

        StringRequest stringRequest = new StringRequest(strGetFoodListUrl, new Response.Listener<String>() {
            @Override
            public void onResponse(String response) {
                try {
                    JSONArray jsonArray = new JSONArray(response);
                    for (int i = 0; i <= jsonArray.length(); i++) {

                        JSONObject jsonObject = jsonArray.getJSONObject(i);

                        int mainFoodId = jsonObject.getInt("id");
                        String mainFoodMain = jsonObject.getString("name");

                        JSONObject jsonObjectCategory = jsonObject.getJSONObject("food_category");
                        int categoryFoodId = jsonObjectCategory.getInt("id");
                        String categoryFoodName = jsonObjectCategory.getString("name");

                        JSONObject jsonObjectServing = jsonObject.getJSONObject("food_serving_size");
                        int servingId = jsonObjectServing.getInt("id");
                        String servingName = jsonObjectServing.getString("name");

                        sharedPreferencesManager.setEditorFoodMain(String.valueOf(mainFoodId), mainFoodMain);
                        sharedPreferencesManager.setEditorFoodCategory(String.valueOf(categoryFoodId), categoryFoodName);
                        sharedPreferencesManager.setEditorFoodServing(String.valueOf(servingId), servingName);
                    }

                } catch (JSONException e) {

                }
            }
        }, new Response.ErrorListener() {
            @Override
            public void onErrorResponse(VolleyError error) {

            }
        });

        requestQueue.add(stringRequest);


    }

    //gets all product information and stores in database
    private void getDrinkListVolley() {
        requestQueue = Volley.newRequestQueue(MainActivity.this);

        String strGetDrinkListUrl = StringConfig.BASE_URL + StringConfig.ROUTE_SHOWALL_DRINK;

        StringRequest stringRequest = new StringRequest(strGetDrinkListUrl, new Response.Listener<String>() {
            @Override
            public void onResponse(String response) {
                try {
                    JSONArray jsonArray = new JSONArray(response);
                    for (int i = 0; i <= jsonArray.length(); i++) {

                        JSONObject jsonObject = jsonArray.getJSONObject(i);

                        int mainDrinkId = jsonObject.getInt("id");
                        String mainDrinkMain = jsonObject.getString("name");

                        JSONObject jsonObjectCategory = jsonObject.getJSONObject("drink_category");
                        int categoryDrinkId = jsonObjectCategory.getInt("id");
                        String categoryDrinkName = jsonObjectCategory.getString("name");

                        JSONObject jsonObjectServing = jsonObject.getJSONObject("drink_serving_temperature");
                        int servingId = jsonObjectServing.getInt("id");
                        String servingName = jsonObjectServing.getString("name");


                        sharedPreferencesManager.setEditorDrinkMain(String.valueOf(mainDrinkId), mainDrinkMain);
                        sharedPreferencesManager.setEditorDrinkCategory(String.valueOf(categoryDrinkId), categoryDrinkName);
                        sharedPreferencesManager.setEditorDrinkServing(String.valueOf(servingId), servingName);

                        //sqLiteCBLCAdapter.insertDataForDrinks(mainDrinkId, mainDrinkMain, categoryDrinkId, categoryDrinkName, servingId, servingName);

                        //String mainId = jsonObject.get
                    }

                } catch (JSONException e) {

                }
            }
        }, new Response.ErrorListener() {
            @Override
            public void onErrorResponse(VolleyError error) {

            }
        });

        requestQueue.add(stringRequest);


    }

    // init food recyclerview adapter and cardview_recyclerholder_foods manager
    private void initFoodRecyclerView() {
        rParserFoodsList.clear();
        rAdapterFoods = new RAdapterFoods(this, rParserFoodsList);
        layoutManagerFoods = new LinearLayoutManager(this, LinearLayoutManager.VERTICAL, false);
        rv_foods.setLayoutManager(layoutManagerFoods);
        rv_foods.setAdapter(rAdapterFoods);
        rv_foods.addItemDecoration(new DividerItemDecoration(this, DividerItemDecoration.VERTICAL));
        requestOrderFoodVolley(requestFood);
    }

    // fetch ordered foods responses
    private void requestOrderFoodVolley(final String requestFood) {
        requestQueue = Volley.newRequestQueue(MainActivity.this);

        String strRequestOrdersVolley = StringConfig.BASE_URL + StringConfig.ROUTE_SHOWORDERS;
        //String strRequestOrdersVolley = "http://192.168.43.91/CBLCAdmin/ShowOrder/showorder.json";

        Map<String, String> mapGetOrder = new HashMap<String, String>();
        mapGetOrder.put("orderType", requestFood);

        CustomRequest customRequest = new CustomRequest(Request.Method.POST, strRequestOrdersVolley, (mapGetOrder != null) ? new JSONObject(mapGetOrder) : null, new Response.Listener<JSONArray>() {

            //StringRequest stringRequest = new StringRequest(strRequestOrdersVolley, new Response.Listener<String>() {
            @Override
            public void onResponse(JSONArray response) {

                try {
                    displayDataToFoodTextViews(response);

                    for (int i = 0; i < response.length(); i++) {

                        Log.d(StringConfig.LOG_TAG, response.toString());
                        JSONObject jsonObject = response.getJSONObject(i);

                        String jsonOrderIsServed = String.valueOf(jsonObject.getBoolean("is_served?"));
                        String jsonOrderIsCancelled = String.valueOf(jsonObject.getBoolean("is_canceled?"));
                        String jsonOrderIsPaid = String.valueOf(jsonObject.getBoolean("is_paid?"));

                        if (jsonOrderIsServed.equals("false") && jsonOrderIsCancelled.equals("false") && jsonOrderIsPaid.equals("true")) {

                            int orderId = jsonObject.getInt("id");
                            int orderFoodId = jsonObject.getInt("food_id");
                            int customerId = jsonObject.getInt("customer_id");
                            int orderQuantity = jsonObject.getInt("quantity");
                            int orderFoodCategoryId = jsonObject.getInt("food_category_id");
                            int orderFoodServingId = jsonObject.getInt("food_serving_size_id");
                            boolean orderIsServed = jsonObject.getBoolean("is_served?");
                            boolean orderIsPaid = jsonObject.getBoolean("is_paid?");
                            boolean orderIsCancelled = jsonObject.getBoolean("is_canceled?");
                            boolean orderIsDiscounted = jsonObject.getBoolean("is_discounted?");
                            String orderDiscount = jsonObject.getString("discount");

                            Log.d(StringConfig.LOG_TAG, "orderId : " + orderId);
                            Log.d(StringConfig.LOG_TAG, "orderFoodId : " + orderFoodId);
                            Log.d(StringConfig.LOG_TAG, "customerId : " + customerId);
                            Log.d(StringConfig.LOG_TAG, "orderQuantity : " + orderQuantity);
                            Log.d(StringConfig.LOG_TAG, "orderFoodCategoryId : " + orderFoodCategoryId);
                            Log.d(StringConfig.LOG_TAG, "orderFoodServingId : " + orderFoodServingId);
                            Log.d(StringConfig.LOG_TAG, "orderIsDiscountedFoods : " + orderIsDiscounted);
                            Log.d(StringConfig.LOG_TAG, "orderDiscountFoods : " + orderDiscount);


                            RParserFoods rParserFoods = new RParserFoods(orderId, orderFoodId, customerId, orderQuantity, orderFoodCategoryId, orderFoodServingId, orderIsServed, orderIsPaid, orderIsCancelled, orderIsDiscounted, orderDiscount);
                            rParserFoodsList.add(rParserFoods);
                            rAdapterFoods.notifyDataSetChanged();

                        } else {
                            continue;
                        }

                    }
                } catch (JSONException e) {

                }
            }
        }, new Response.ErrorListener() {
            @Override
            public void onErrorResponse(VolleyError error) {
                volleyNetworkRequest.volleyErrorLogger(error, cl_main);
            }
        });
        requestQueue.add(customRequest);

        cardview_holder_foods.setVisibility(View.GONE);
        contentholder_order_foods.setVisibility(View.GONE);

    }

    private void displayDataToFoodTextViews(JSONArray response) {
        try {

            if (response.length() == 0) {
                cardview_holder_foods.setVisibility(View.GONE);
                contentholder_order_foods.setVisibility(View.GONE);

                Snackbar snackbar = Snackbar.make(srl_foods, "No pending food orders.", Snackbar.LENGTH_SHORT);
                snackbar.show();

            } else {

                cardview_holder_foods.setVisibility(View.VISIBLE);
                contentholder_order_foods.setVisibility(View.VISIBLE);

                for (int i = 0; i < response.length(); i++) {

                    JSONObject jsonObjectFoods = response.getJSONObject(i);

                    String isServedAtIndex0 = String.valueOf(jsonObjectFoods.getBoolean("is_served?"));
                    String isCancelledAtIndex0 = String.valueOf(jsonObjectFoods.getBoolean("is_canceled?"));
                    String isPaidAtIndex0 = String.valueOf(jsonObjectFoods.getBoolean("is_paid?"));

                    if (isServedAtIndex0.equals("false") && isCancelledAtIndex0.equals("false") && isPaidAtIndex0.equals("true")) {

                        int intJsonOrderIdIndex0 = jsonObjectFoods.getInt("id");
                        int intJsonOrderFoodIdIndex0 = jsonObjectFoods.getInt("food_id");
                        int intJsonCustomerIdIndex0 = jsonObjectFoods.getInt("customer_id");
                        int intJsonQuantityIndex0 = jsonObjectFoods.getInt("quantity");
                        int strJsonOrderCatIndex0 = jsonObjectFoods.getInt("food_category_id");
                        int strJsonOrderSizeIndex0 = jsonObjectFoods.getInt("food_serving_size_id");
                        boolean orderIsDiscounted = jsonObjectFoods.getBoolean("is_discounted?");
                        String orderDiscount = jsonObjectFoods.getString("discount");


                        //String strJsonOrderNameIndex0 = jsonObjectFoods.getString("name");


                        //Log.d(StringConfig.LOG_TAG, "type1 : jsonObjectFoods : jsonId : " + intJsonOrderIdIndex0);
                        //Log.d(StringConfig.LOG_TAG, "type1 : jsonObjectFoods : jsonName : " + strJsonOrderNameIndex0);
                        Log.d(StringConfig.LOG_TAG, "type1 : jsonObjectFoods : customerId : " + intJsonCustomerIdIndex0);

                        Log.d(StringConfig.LOG_TAG, "type1 : jsonObjectFoods : getOrderIdAndOrder category : " + strJsonOrderCatIndex0);
                        Log.d(StringConfig.LOG_TAG, "type1 : jsonObjectFoods : getOrderIdAndOrder size : " + strJsonOrderSizeIndex0);
                        Log.d(StringConfig.LOG_TAG, "type1 : jsonObjectFoods : quantity : " + intJsonQuantityIndex0);
                        Log.d(StringConfig.LOG_TAG, "type1 : jsonObjectFoods : orderIsDiscounted : " + orderIsDiscounted);
                        Log.d(StringConfig.LOG_TAG, "type1 : jsonObjectFoods : orderDiscount : " + orderDiscount);

                        tv_cardFood_customerName.setText(String.valueOf(sharedPreferencesManager.getSharedPreferencesListUser(String.valueOf(intJsonCustomerIdIndex0))));
                        tv_cardFood_orderId.setText(String.valueOf(intJsonOrderIdIndex0));
                        tv_cardFood_orderName.setText(sharedPreferencesManager.getSharedPreferencesFoodMain(String.valueOf(intJsonOrderFoodIdIndex0)));
                        tv_cardFood_orderCategory.setText(sharedPreferencesManager.getSharedPreferencesFoodCategory(String.valueOf(strJsonOrderCatIndex0)));
                        tv_cardFood_orderSize.setText(sharedPreferencesManager.getSharedPreferencesFoodServing(String.valueOf(strJsonOrderSizeIndex0)));
                        tv_cardFood_orderQuantity.setText(String.valueOf(intJsonQuantityIndex0));
                        tv_cardFood_orderIsDiscounted.setText(String.valueOf(orderIsDiscounted));
                        tv_cardFood_orderDiscount.setText(orderDiscount);
                        tv_cardFood_orderFoodId.setText(String.valueOf(intJsonOrderFoodIdIndex0));

                        displayCurrentlyServingFoodOrder();

                        break;


                    } else {
                        continue;
                    }

                }
            }

        } catch (JSONException e) {

        }


    }

    private void initDrinkRecyclerView() {
        rParserDrinksList.clear();
        rAdapterDrinks = new RAdapterDrinks(this, rParserDrinksList);
        layoutManagerDrinks = new LinearLayoutManager(this, LinearLayoutManager.VERTICAL, false);
        rv_drinks.setLayoutManager(layoutManagerDrinks);
        rv_drinks.setAdapter(rAdapterDrinks);
        rv_drinks.addItemDecoration(new DividerItemDecoration(this, DividerItemDecoration.VERTICAL));
        requestOrderDrinkVolley(requestDrink);
    }

    private void requestOrderDrinkVolley(String requestDrink) {
        requestQueue = Volley.newRequestQueue(MainActivity.this);

        String strRequestOrdersVolley = StringConfig.BASE_URL + StringConfig.ROUTE_SHOWORDERS;

        Map<String, String> mapGetOrder = new HashMap<String, String>();
        mapGetOrder.put("orderType", requestDrink);

        //StringRequest stringRequest = new StringRequest(strRequestOrdersVolley, new Response.Listener<String>() {
        CustomRequest customRequest = new CustomRequest(Request.Method.POST, strRequestOrdersVolley, (mapGetOrder != null) ? new JSONObject(mapGetOrder) : null, new Response.Listener<JSONArray>() {
            @Override
            public void onResponse(JSONArray response) {
                try {
                    displayDataToDrinkTextViews(response);
                    Log.d(StringConfig.LOG_TAG, "drinkRequestResponse : " + response);
                    for (int i = 0; i < response.length(); i++) {

                        Log.d(StringConfig.LOG_TAG, response.toString());
                        JSONObject jsonObject = response.getJSONObject(i);

                        String jsonOrderIsServed = String.valueOf(jsonObject.getBoolean("is_served?"));
                        String jsonOrderIsCancelled = String.valueOf(jsonObject.getBoolean("is_canceled?"));
                        String jsonOrderIsPaid = String.valueOf(jsonObject.getBoolean("is_paid?"));

                        if (jsonOrderIsServed.equals("false") && jsonOrderIsCancelled.equals("false") && jsonOrderIsPaid.equals("true")) {

                            int orderId = jsonObject.getInt("id");
                            int orderDrinkId = jsonObject.getInt("drink_id");
                            int customerId = jsonObject.getInt("customer_id");
                            int orderQuantity = jsonObject.getInt("quantity");
                            int orderDrinkCategoryId = jsonObject.getInt("drink_category_id");
                            int orderDrinkServingId = jsonObject.getInt("drink_serving_temperature_id");
                            boolean orderIsServed = jsonObject.getBoolean("is_served?");
                            boolean orderIsPaid = jsonObject.getBoolean("is_paid?");
                            boolean orderIsCancelled = jsonObject.getBoolean("is_canceled?");
                            String orderIsRedeemedWithPoints = jsonObject.getString("is_paid_with_points?");
                            boolean orderIsDiscounted = jsonObject.getBoolean("is_discounted?");
                            String orderDiscount = jsonObject.getString("discount");

                            Log.d(StringConfig.LOG_TAG, "orderId : " + orderId);
                            Log.d(StringConfig.LOG_TAG, "orderDrinkId : " + orderDrinkId);
                            Log.d(StringConfig.LOG_TAG, "customerId : " + customerId);
                            Log.d(StringConfig.LOG_TAG, "orderQuantity : " + orderQuantity);
                            Log.d(StringConfig.LOG_TAG, "orderDrinkCategoryId : " + orderDrinkCategoryId);
                            Log.d(StringConfig.LOG_TAG, "orderDrinkServingId : " + orderDrinkServingId);
                            Log.d(StringConfig.LOG_TAG, "orderIsDiscountedDrinks : " + orderIsDiscounted);
                            Log.d(StringConfig.LOG_TAG, "orderDiscountDrinks : " + orderDiscount);


                            RParserDrinks rParserDrinks = new RParserDrinks(orderId, orderDrinkId, customerId, orderQuantity, orderDrinkCategoryId, orderDrinkServingId, orderIsServed, orderIsPaid, orderIsCancelled, orderIsRedeemedWithPoints, orderIsDiscounted, orderDiscount);
                            rParserDrinksList.add(rParserDrinks);
                            rAdapterDrinks.notifyDataSetChanged();

                        } else {
                            continue;
                        }

                    }
                } catch (JSONException e) {

                }
            }
        }, new Response.ErrorListener() {
            @Override
            public void onErrorResponse(VolleyError error) {
                volleyNetworkRequest.volleyErrorLogger(error, cl_main);
            }
        });

        requestQueue.add(customRequest);
        cardview_holder_drinks.setVisibility(View.GONE);
        contentholder_order_drinks.setVisibility(View.GONE);


    }

    private void displayDataToDrinkTextViews(JSONArray response) {
        try {

            if (response.length() == 0) {
                cardview_holder_drinks.setVisibility(View.GONE);
                contentholder_order_drinks.setVisibility(View.GONE);

                Snackbar snackbar = Snackbar.make(srl_drinks, "No pending drink orders.", Snackbar.LENGTH_SHORT);
                snackbar.show();


            } else {

                cardview_holder_drinks.setVisibility(View.VISIBLE);
                contentholder_order_drinks.setVisibility(View.VISIBLE);

                for (int i = 0; i < response.length(); i++) {

                    JSONObject jsonObjectDrinks = response.getJSONObject(i);

                    String isServedAtIndex0 = String.valueOf(jsonObjectDrinks.getBoolean("is_served?"));
                    String isCancelledAtIndex0 = String.valueOf(jsonObjectDrinks.getBoolean("is_canceled?"));
                    String isPaidAtIndex0 = String.valueOf(jsonObjectDrinks.getBoolean("is_paid?"));

                    if (isServedAtIndex0.equals("false") && isCancelledAtIndex0.equals("false") && isPaidAtIndex0.equals("true")) {

                        int intJsonOrderIdIndex0 = jsonObjectDrinks.getInt("id");
                        int intJsonOrderDrinkIdIndex0 = jsonObjectDrinks.getInt("drink_id");
                        int intJsonCustomerIdIndex0 = jsonObjectDrinks.getInt("customer_id");
                        int intJsonQuantityIndex0 = jsonObjectDrinks.getInt("quantity");
                        int strJsonOrderCatIndex0 = jsonObjectDrinks.getInt("drink_category_id");
                        int strJsonOrderSizeIndex0 = jsonObjectDrinks.getInt("drink_serving_temperature_id");
                        String orderIsRedeemedWithPoints = jsonObjectDrinks.getString("is_paid_with_points?");
                        boolean orderIsDiscounted = jsonObjectDrinks.getBoolean("is_discounted?");
                        String orderDiscount = jsonObjectDrinks.getString("discount");


                        //String strJsonOrderNameIndex0 = jsonObjectDrinks.getString("name");


                        //Log.d(StringConfig.LOG_TAG, "type1 : jsonObjectDrinks : jsonId : " + intJsonOrderIdIndex0);
                        //Log.d(StringConfig.LOG_TAG, "type1 : jsonObjectDrinks : jsonName : " + strJsonOrderNameIndex0);
                        Log.d(StringConfig.LOG_TAG, "type1 : jsonObjectDrinks : customerId : " + intJsonCustomerIdIndex0);

                        Log.d(StringConfig.LOG_TAG, "type1 : jsonObjectDrinks : getOrderIdAndOrder category : " + strJsonOrderCatIndex0);
                        Log.d(StringConfig.LOG_TAG, "type1 : jsonObjectDrinks : getOrderIdAndOrder size : " + strJsonOrderSizeIndex0);
                        Log.d(StringConfig.LOG_TAG, "type1 : jsonObjectDrinks : quantity : " + intJsonQuantityIndex0);
                        Log.d(StringConfig.LOG_TAG, "type1 : jsonObjectDrinks : orderIsDiscounted : " + orderIsDiscounted);
                        Log.d(StringConfig.LOG_TAG, "type1 : jsonObjectDrinks : orderDiscount : " + orderDiscount);

                        tv_cardDrink_customerName.setText(String.valueOf(sharedPreferencesManager.getSharedPreferencesListUser(String.valueOf(intJsonCustomerIdIndex0))));
                        tv_cardDrink_orderId.setText(String.valueOf(intJsonOrderIdIndex0));
                        tv_cardDrink_orderName.setText(sharedPreferencesManager.getSharedPreferencesDrinkMain(String.valueOf(intJsonOrderDrinkIdIndex0)));
                        tv_cardDrink_orderCategory.setText(sharedPreferencesManager.getSharedPreferencesDrinkCategory(String.valueOf(strJsonOrderCatIndex0)));
                        tv_cardDrink_orderSize.setText(sharedPreferencesManager.getSharedPreferencesDrinkServing(String.valueOf(strJsonOrderSizeIndex0)));
                        tv_cardDrink_orderQuantity.setText(String.valueOf(intJsonQuantityIndex0));
                        tv_cardDrink_isPaidWithPoints.setText(orderIsRedeemedWithPoints);
                        tv_cardDrink_orderIsDiscounted.setText(String.valueOf(orderIsDiscounted));
                        tv_cardDrink_orderDiscount.setText(orderDiscount);
                        tv_cardDrink_orderDrinkId.setText(String.valueOf(intJsonOrderDrinkIdIndex0));

                        displayCurrentlyServingDrinkOrder();

                        break;


                    } else {
                        continue;
                    }

                }
            }
        } catch (JSONException e) {

        }

    }

    private void displayCurrentlyServingFoodOrder() {

        String currentFoodOrderName = tv_cardFood_orderName.getText().toString();
        String currentFoodOrderQuantity = tv_cardFood_orderQuantity.getText().toString();
        String currentFoodOrderSize = tv_cardFood_orderSize.getText().toString();

        tv_confirmServedFood_orderName.setText(currentFoodOrderName);
        tv_confirmServedFood_orderQuantity.setText(currentFoodOrderQuantity);

        sharedPreferencesManager.clearSharedPreferenceOrderQueueFood();
        sharedPreferencesManager.setEditorOrderQueueFood(currentFoodOrderName, currentFoodOrderQuantity);

    }

    private void displayCurrentlyServingDrinkOrder() {

        String currentDrinkOrderName = tv_cardDrink_orderName.getText().toString();
        String currentDrinkOrderQuantity = tv_cardDrink_orderQuantity.getText().toString();
        String currentDrinkOrderTemp = tv_cardDrink_orderSize.getText().toString();

        tv_confirmServedDrink_orderName.setText(currentDrinkOrderName);
        tv_confirmServedDrink_orderQuantity.setText(currentDrinkOrderQuantity);

        sharedPreferencesManager.clearSharedPreferenceOrderQueueDrink();
        sharedPreferencesManager.setEditorOrderQueueDrink(currentDrinkOrderName, currentDrinkOrderQuantity);
    }

    public void btn_servedFood(View view) {
        try {
            String strServeOrderUrl = StringConfig.BASE_URL + StringConfig.SERVED_ORDER;
            String getOrderName = tv_confirmServedFood_orderName.getText().toString();
            String type = "food";
            String getOrderIsDiscounted = tv_cardFood_orderIsDiscounted.getText().toString();
            String getOrderDiscount = tv_cardFood_orderDiscount.getText().toString();

            int getOrderQuantity = Integer.parseInt(sharedPreferencesManager.getSharedPreferenceOrderQueueFood(getOrderName));

            int orderId = Integer.parseInt(tv_cardFood_orderId.getText().toString());

            if (getOrderQuantity > 1) {
                getOrderQuantity--;
                //int sharedPrefQuantityValue = Integer.parseInt(sharedPreferencesManager.getSharedPreferenceOrderQueueFood(orderName)) - 1;

                sharedPreferencesManager.setEditorOrderQueueFood(getOrderName, String.valueOf(getOrderQuantity));
                tv_confirmServedFood_orderQuantity.setText(sharedPreferencesManager.getSharedPreferenceOrderQueueFood(getOrderName));

            } else if (getOrderQuantity == 1) {
                getOrderIdAndOrder(strServeOrderUrl, type, getOrderQuantity, getOrderIsDiscounted, getOrderDiscount);
            }

        } catch (NumberFormatException e) {

        }
    }

    public void btn_servedDrink(View view) {
        try {

            String strServeOrderUrl = StringConfig.BASE_URL + StringConfig.SERVED_ORDER;
            String getOrderName = tv_confirmServedDrink_orderName.getText().toString();
            String getOrderIsDiscounted = tv_cardDrink_orderIsDiscounted.getText().toString();
            String getOrderDiscount = tv_cardDrink_orderDiscount.getText().toString();

            int getOrderQuantity = Integer.parseInt(sharedPreferencesManager.getSharedPreferenceOrderQueueDrink(getOrderName));
            String type = "drink";

            if (getOrderQuantity > 1) {
                getOrderQuantity--;
                //int sharedPrefQuantityValue = Integer.parseInt(sharedPreferencesManager.getSharedPreferenceOrderQueueFood(orderName)) - 1;

                sharedPreferencesManager.setEditorOrderQueueDrink(getOrderName, String.valueOf(getOrderQuantity));
                tv_confirmServedDrink_orderQuantity.setText(sharedPreferencesManager.getSharedPreferenceOrderQueueDrink(getOrderName));

            } else if (getOrderQuantity == 1) {
                getOrderIdAndOrder(strServeOrderUrl, type, getOrderQuantity, getOrderIsDiscounted, getOrderDiscount);
            }

        } catch (NumberFormatException e) {

        }

    }

    public void btn_cancelFood(View view) {

        String orderName = tv_cardFood_orderName.getText().toString();
        final String getOrderIsDiscounted = tv_cardFood_orderIsDiscounted.getText().toString();
        final String getOrderDiscount = tv_cardFood_orderDiscount.getText().toString();

        if (!(orderName.isEmpty() || orderName.length() == 0)) {

            final AlertDialog.Builder builder = new AlertDialog.Builder(MainActivity.this);
            builder.setMessage("Are you sure you want to cancel the order " + orderName)
                    .setPositiveButton("Yes", new DialogInterface.OnClickListener() {
                        @Override
                        public void onClick(DialogInterface dialogInterface, int i) {
                            String strCancelOrderUrl = StringConfig.BASE_URL + StringConfig.CANCEL_ORDER;
                            try {
                                int getOrderQuantity = Integer.parseInt(tv_confirmServedFood_orderQuantity.getText().toString());

                                String type = "food";
                                getOrderIdAndOrder(strCancelOrderUrl, type, getOrderQuantity, getOrderIsDiscounted, getOrderDiscount);
                            } catch (NumberFormatException e) {

                            }
                        }
                    })
                    .setNegativeButton("No", new DialogInterface.OnClickListener() {
                        @Override
                        public void onClick(DialogInterface dialogInterface, int i) {

                        }
                    });


            AlertDialog alertDialog = builder.create();
            alertDialog.show();
        }

    }

    public void btn_cancelDrink(View view) {
        String orderName = tv_cardDrink_orderName.getText().toString();
        final String getOrderIsDiscounted = tv_cardDrink_orderIsDiscounted.getText().toString();
        final String getOrderDiscount = tv_cardDrink_orderDiscount.getText().toString();

        if (!(orderName.isEmpty() || orderName.length() == 0)) {

            AlertDialog.Builder builder = new AlertDialog.Builder(MainActivity.this);
            builder.setMessage("Are you sure you want to cancel the order " + orderName)
                    .setPositiveButton("Yes", new DialogInterface.OnClickListener() {
                        @Override
                        public void onClick(DialogInterface dialogInterface, int i) {
                            String strCancelOrderUrl = StringConfig.BASE_URL + StringConfig.CANCEL_ORDER;

                            try {
                                int getOrderQuantity = Integer.parseInt(tv_confirmServedDrink_orderQuantity.getText().toString());
                                String type = "drink";
                                getOrderIdAndOrder(strCancelOrderUrl, type, getOrderQuantity, getOrderIsDiscounted, getOrderDiscount);
                            } catch (NumberFormatException e) {

                            }
                        }
                    })
                    .setNegativeButton("No", new DialogInterface.OnClickListener() {
                        @Override
                        public void onClick(DialogInterface dialogInterface, int i) {

                        }
                    });

            AlertDialog alertDialog = builder.create();
            alertDialog.show();

        }

    }

    private void getOrderIdAndOrder(String strCancelOrServeOrderUrl, String type, int getOrderQuantity, String orderIsDiscounted, String orderDiscount) {
        try {
            if (type.contains("food")) {
                int orderId = Integer.parseInt(tv_cardFood_orderId.getText().toString());
                String orderName = tv_cardFood_orderName.getText().toString();
                String orderQuantity = tv_cardFood_orderQuantity.getText().toString();

                /*if (getOrderQuantity > 1) {
                    getOrderQuantity--;
                    //int sharedPrefQuantityValue = Integer.parseInt(sharedPreferencesManager.getSharedPreferenceOrderQueueFood(orderName)) - 1;

                    sharedPreferencesManager.setEditorOrderQueueFood(orderName, String.valueOf(getOrderQuantity));
                    tv_confirmServedFood_orderQuantity.setText(sharedPreferencesManager.getSharedPreferenceOrderQueueFood(orderName));

                } else if (getOrderQuantity == 1) {
                    serveOrCancelOrder(orderId, strCancelOrServeOrderUrl, orderName, "Food");
                }*/

                serveOrCancelOrder(orderId, strCancelOrServeOrderUrl, orderName, "Food", "false", orderIsDiscounted, orderDiscount, orderQuantity, tv_cardFood_orderFoodId.getText().toString());
            }

            if (type.contains("drink")) {
                int orderId = Integer.parseInt(tv_cardDrink_orderId.getText().toString());
                String orderName = tv_cardDrink_orderName.getText().toString();
                String orderQuantity = tv_cardDrink_orderQuantity.getText().toString();
                String isPaidWithPoints = tv_cardDrink_isPaidWithPoints.getText().toString();

                /*if (getOrderQuantity > 1) {

                    getOrderQuantity--;

                    sharedPreferencesManager.setEditorOrderQueueDrink(orderName, String.valueOf(getOrderQuantity));
                    tv_confirmServedDrink_orderQuantity.setText(String.valueOf(getOrderQuantity));


                } else if (getOrderQuantity == 1) {*/
                serveOrCancelOrder(orderId, strCancelOrServeOrderUrl, orderName, "Drink", isPaidWithPoints, orderIsDiscounted, orderDiscount, orderQuantity, tv_cardDrink_orderDrinkId.getText().toString());
                //}
            }
        } catch (NumberFormatException e) {
            //Toast.makeText(MainActivity.this, "No orders to be served or cancelled.", Toast.LENGTH_SHORT).show();
            if (strCancelOrServeOrderUrl.contains("serve")) {
                Snackbar snackbar = Snackbar.make(cl_main, "No orders to be served.", Snackbar.LENGTH_SHORT);
                snackbar.show();
            } else if (strCancelOrServeOrderUrl.contains("cancel")) {
                Snackbar snackbar = Snackbar.make(cl_main, "No orders to be cancelled.", Snackbar.LENGTH_SHORT);
                snackbar.show();
            } else {
                Snackbar snackbar = Snackbar.make(cl_main, "No orders to be cancelled or served.", Snackbar.LENGTH_SHORT);
                snackbar.show();
            }
        }
    }

    private void serveOrCancelOrder(final Integer orderId, final String url, final String orderName, final String orderType, final String isPaidWithPoints, final String orderIsDiscounted, final String orderDiscount, final String orderQuantity, final String orderedFoodOrDrinkId) {
        Log.d(StringConfig.LOG_TAG, "blablablabl orderId : " + orderId);
        Log.d(StringConfig.LOG_TAG, "blablablabl url : " + url);
        Log.d(StringConfig.LOG_TAG, "blablablabl orderName : " + orderName);
        Log.d(StringConfig.LOG_TAG, "blablablabl orderrType : " + orderType);
        Log.d(StringConfig.LOG_TAG, "blablablabl orderIsDiscounted : " + orderIsDiscounted);
        Log.d(StringConfig.LOG_TAG, "blablablabl orderDiscount : " + orderDiscount);
        Log.d(StringConfig.LOG_TAG, "blablablabl orderedFoodOrDrinkId : " + orderedFoodOrDrinkId);

        requestQueue = Volley.newRequestQueue(MainActivity.this);

        Map<String, String> mapServe = new HashMap<String, String>();
        mapServe.put("orderType", orderType);
        mapServe.put("id", String.valueOf(orderId));
        mapServe.put("is_discounted?", orderIsDiscounted);
        mapServe.put("discount", orderDiscount);

        JsonObjectRequest jsonObjectRequest = new JsonObjectRequest(Request.Method.POST, url, new JSONObject(mapServe), new Response.Listener<JSONObject>() {
            @Override
            public void onResponse(JSONObject response) {
                Log.d(StringConfig.LOG_TAG, "served response : " + response.toString());
                try {
                    String strJsonObjectResult = response.getString("result");

                    if (strJsonObjectResult.equals("success")) {

                        if (url.contains("cancel")) {
                            if (orderType.contains("Food")) {
                                Snackbar snackbar = Snackbar.make(srl_foods, orderName + " cancelled.", Snackbar.LENGTH_SHORT);
                                snackbar.show();

                                clearFoodTextViewContents();

                                refreshFoods();
                            } else {
                                Snackbar snackbar = Snackbar.make(srl_drinks, orderName + " cancelled.", Snackbar.LENGTH_SHORT);
                                snackbar.show();

                                clearDrinkTextViewContents();

                                refreshDrinks();
                            }

                        } else if (url.contains("serve")) {
                            if (isPaidWithPoints.equals("false")) {
                                if (orderType.contains("Food")) {
                                    Snackbar snackbar = Snackbar.make(srl_foods, orderName + " served.", Snackbar.LENGTH_SHORT);
                                    snackbar.show();

                                    clearFoodTextViewContents();

                                    refreshFoods();

                                    postToSales("Food", String.valueOf(orderId), orderIsDiscounted, orderDiscount, orderQuantity);
                                } else {
                                    Snackbar snackbar = Snackbar.make(srl_drinks, orderName + " served.", Snackbar.LENGTH_SHORT);
                                    snackbar.show();

                                    clearDrinkTextViewContents();
                                    refreshDrinks();

                                    postToSales("Drink", String.valueOf(orderId), orderIsDiscounted, orderDiscount, orderQuantity);
                                }

                                Log.d(StringConfig.LOG_TAG, "serveOrderFromServeOrCancelOrder : " + orderType);


                                if (orderType.contains("Food")) {
                                    updateInventory("inventoryType", orderType, "food_id", String.valueOf(orderId), "quantity", orderQuantity, orderedFoodOrDrinkId);
                                } else {
                                    updateInventory("inventoryType", orderType, "drink_id", String.valueOf(orderId), "quantity", orderQuantity, orderedFoodOrDrinkId);
                                }

                            } else {
                                Snackbar snackbar = Snackbar.make(srl_drinks, orderName + " served.", Snackbar.LENGTH_SHORT);
                                snackbar.show();

                                clearDrinkTextViewContents();
                                refreshDrinks();
                            }
                        }

                    } else {
                        Snackbar snackbar = Snackbar.make(cl_main, "Failed", Snackbar.LENGTH_SHORT);
                        snackbar.show();
                    }

                } catch (JSONException e) {

                }


            }
        }, new Response.ErrorListener() {
            @Override
            public void onErrorResponse(VolleyError error) {
                volleyNetworkRequest.volleyErrorLogger(error, cl_main);
            }
        });

        requestQueue.add(jsonObjectRequest);

    }

    private void postToSales(final String orderType, final String orderId, String orderIsDiscounted, String orderDiscount, final String orderQuantity) {
        requestQueue = Volley.newRequestQueue(MainActivity.this);
        Log.d(StringConfig.LOG_TAG, "test orderIsDiscounted : " + orderIsDiscounted);
        Log.d(StringConfig.LOG_TAG, "test orderDiscount : " + orderDiscount);
        Log.d(StringConfig.LOG_TAG, "test orderType : " + orderType);


        String strPostToSaledUrl = StringConfig.BASE_URL + StringConfig.ROUTE_SALES;

        Map<String, String> mapPostToSales = new HashMap<String, String>();
        if (orderType.contains("Food")) {
            mapPostToSales.put("saleType", orderType);
            mapPostToSales.put("ordered_food_id", orderId);
            mapPostToSales.put("is_discounted?", orderIsDiscounted);
            mapPostToSales.put("discount", orderDiscount);
        } else {
            mapPostToSales.put("saleType", orderType);
            mapPostToSales.put("ordered_drink_id", orderId);
            mapPostToSales.put("is_discounted?", orderIsDiscounted);
            mapPostToSales.put("discount", orderDiscount);
        }

        JsonObjectRequest jsonObjectRequest = new JsonObjectRequest(Request.Method.POST, strPostToSaledUrl, new JSONObject(mapPostToSales), new Response.Listener<JSONObject>() {
            @Override
            public void onResponse(JSONObject response) {
                Log.d(StringConfig.LOG_TAG, "sales response : " + response);
                //try {
                    /*JSONObject jsonObjectResult = response.getJSONObject("result");
                    Log.d(StringConfig.LOG_TAG, "jsonObjectREsult sales response : " + jsonObjectResult);*/

                    postToDailySales(orderType);

/*
                } catch (JSONException e) {

                }*/
            }

        }, new Response.ErrorListener() {
            @Override
            public void onErrorResponse(VolleyError error) {
                volleyNetworkRequest.volleyErrorLogger(error, cl_main);
            }
        });


        requestQueue.add(jsonObjectRequest);
    }

    private void postToDailySales(String orderType) {
        requestQueue = Volley.newRequestQueue(MainActivity.this);

        String strPostToDailySales = StringConfig.BASE_URL + StringConfig.ROUTE_DAILY_SALES;

        Map<String, String> mapPostToDailySales = new HashMap<String, String>();
        mapPostToDailySales.put("saleType", orderType);

        JsonObjectRequest jsonObjectRequest = new JsonObjectRequest(Request.Method.POST, strPostToDailySales, new JSONObject(mapPostToDailySales), new Response.Listener<JSONObject>() {
            @Override
            public void onResponse(JSONObject response) {
                Log.d(StringConfig.LOG_TAG, "postToDailySales response : " + response.toString());
            }
        }, new Response.ErrorListener() {
            @Override
            public void onErrorResponse(VolleyError error) {

            }
        });

        requestQueue.add(jsonObjectRequest);
    }


    public void updateInventory(String inventoryType, String valueOrderType, String paramsOrderId, String valueOrderId, String paramsOrderQuantity, String valueOrderQuantity, String orderedFoodOrDrinkId) {
        Log.d(StringConfig.LOG_TAG, "updateInventory : inventoryType : " + inventoryType);
        Log.d(StringConfig.LOG_TAG, "updateInventory : valueOrderType : " + valueOrderType);
        Log.d(StringConfig.LOG_TAG, "updateInventory : paramsOrderId : " + paramsOrderId);
        Log.d(StringConfig.LOG_TAG, "updateInventory : valueOrderId : " + orderedFoodOrDrinkId);
        Log.d(StringConfig.LOG_TAG, "updateInventory : paramsOrderQuantity : " + paramsOrderQuantity);
        Log.d(StringConfig.LOG_TAG, "updateInventory : valueOrderQuantity : " + valueOrderQuantity);

        RequestQueue requestQueue = Volley.newRequestQueue(MainActivity.this);

        String strUpdateInventoryUrl = StringConfig.BASE_URL + StringConfig.ROUTE_UPDATE_INVENTORY;

        Map<String, String> mapUpdateInventory = new HashMap<String, String>();
        mapUpdateInventory.put(inventoryType, valueOrderType);
        mapUpdateInventory.put(paramsOrderId, orderedFoodOrDrinkId);
        mapUpdateInventory.put(paramsOrderQuantity, valueOrderQuantity);

        JsonObjectRequest jsonObjectRequest = new JsonObjectRequest(Request.Method.POST, strUpdateInventoryUrl, new JSONObject(mapUpdateInventory), new Response.Listener<JSONObject>() {
            @Override
            public void onResponse(JSONObject response) {
                Log.d(StringConfig.LOG_TAG, "response");
            }
        }, new Response.ErrorListener() {
            @Override
            public void onErrorResponse(VolleyError error) {

            }
        });

        requestQueue.add(jsonObjectRequest);
    }

}
