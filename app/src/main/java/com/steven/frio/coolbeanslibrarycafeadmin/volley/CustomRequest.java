package com.steven.frio.coolbeanslibrarycafeadmin.volley;

import com.android.volley.NetworkResponse;
import com.android.volley.ParseError;
import com.android.volley.Response;
import com.android.volley.toolbox.HttpHeaderParser;
import com.android.volley.toolbox.JsonRequest;

import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

import java.io.UnsupportedEncodingException;

/**
 * Created by steven on 10/2/17.
 */

public class CustomRequest extends JsonRequest<JSONArray>{

    public CustomRequest(String url, String requestBody, Response.Listener<JSONArray> listener, Response.ErrorListener errorListener) {
        super(url, requestBody, listener, errorListener);
    }


    public CustomRequest(int post, String strLoadFoodSubCatURL, JSONObject jsonObject, Response.Listener<JSONArray> listener, Response.ErrorListener errorListener) {
        super(post, strLoadFoodSubCatURL, String.valueOf(jsonObject), listener, errorListener);

    }


    @Override
    protected Response<JSONArray> parseNetworkResponse(NetworkResponse response) {
        try {
            String jsonString = new String(response.data,
                    HttpHeaderParser.parseCharset(response.headers, PROTOCOL_CHARSET));
            return Response.success(new JSONArray(jsonString),
                    HttpHeaderParser.parseCacheHeaders(response));
        } catch (UnsupportedEncodingException e) {
            return Response.error(new ParseError(e));
        } catch (JSONException je) {
            return Response.error(new ParseError(je));
        }
    }
}
