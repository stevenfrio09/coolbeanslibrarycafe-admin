package com.steven.frio.coolbeanslibrarycafeadmin.recyclerparsers;

/**
 * Created by steven on 10/1/17.
 */

public class RParserFoods {
    private Integer orderId;
    private Integer orderFoodId;
    private Integer customerId;
    private Integer orderQuantityId;
    private Integer orderFoodCategoryId;
    private Integer orderFoodServingId;
    private boolean orderIsServed;
    private boolean orderIsPaid;
    private boolean orderIsCancelled;
    private boolean orderIsDiscounted;
    private String orderDiscount;

    public RParserFoods(Integer orderId, Integer orderFoodId, Integer customerId, Integer orderQuantityId, Integer orderFoodCategoryId, Integer orderFoodServingId, Boolean orderIsServed,Boolean orderIsPaid, Boolean orderIsCancelled, Boolean orderIsDiscounted, String orderDiscount){
        this.orderId = orderId;
        this.orderFoodId = orderFoodId;
        this.customerId = customerId;
        this.orderQuantityId = orderQuantityId;
        this.orderFoodCategoryId = orderFoodCategoryId;
        this.orderFoodServingId = orderFoodServingId;
        this.orderIsServed = orderIsServed;
        this.orderIsPaid = orderIsPaid;
        this.orderIsCancelled = orderIsCancelled;
        this.orderIsDiscounted = orderIsDiscounted;
        this.orderDiscount = orderDiscount;
    }

    public Integer getOrderId() {
        return orderId;
    }

    public void setOrderId(Integer orderId) {
        this.orderId = orderId;
    }

    public Integer getOrderFoodId() {
        return orderFoodId;
    }

    public void setOrderFoodId(Integer orderFoodId) {
        this.orderFoodId = orderFoodId;
    }

    public Integer getCustomerId() {
        return customerId;
    }

    public void setCustomerId(Integer customerId) {
        this.customerId = customerId;
    }

    public Integer getOrderQuantityId() {
        return orderQuantityId;
    }

    public void setOrderQuantityId(Integer orderQuantityId) {
        this.orderQuantityId = orderQuantityId;
    }

    public Integer getOrderFoodCategoryId() {
        return orderFoodCategoryId;
    }

    public void setOrderFoodCategoryId(Integer orderFoodCategoryId) {
        this.orderFoodCategoryId = orderFoodCategoryId;
    }

    public Integer getOrderFoodServingId() {
        return orderFoodServingId;
    }

    public void setOrderFoodServingId(Integer orderFoodServingId) {
        this.orderFoodServingId = orderFoodServingId;
    }

    public boolean isOrderIsServed() {
        return orderIsServed;
    }

    public void setOrderIsServed(boolean orderIsServed) {
        this.orderIsServed = orderIsServed;
    }

    public boolean isOrderIsPaid() {
        return orderIsPaid;
    }

    public void setOrderIsPaid(boolean orderIsPaid) {
        this.orderIsPaid = orderIsPaid;
    }

    public boolean isOrderIsCancelled() {
        return orderIsCancelled;
    }

    public void setOrderIsCancelled(boolean orderIsCancelled) {
        this.orderIsCancelled = orderIsCancelled;
    }

    public boolean isOrderIsDiscounted() {
        return orderIsDiscounted;
    }

    public void setOrderIsDiscounted(boolean orderIsDiscounted) {
        this.orderIsDiscounted = orderIsDiscounted;
    }

    public String getOrderDiscount() {
        return orderDiscount;
    }

    public void setOrderDiscount(String orderDiscount) {
        this.orderDiscount = orderDiscount;
    }
}
