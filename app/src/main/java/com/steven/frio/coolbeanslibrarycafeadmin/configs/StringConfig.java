package com.steven.frio.coolbeanslibrarycafeadmin.configs;

import com.steven.frio.coolbeanslibrarycafeadmin.sharedpreferences.SharedPreferencesManager;

/**
 * Created by steven on 9/12/17.
 */

public class StringConfig {
    public static final String LOG_TAG = "SAD";

    //public static final String BASE_URL = "http://192.168.43.156:3000/";

    public static final String BASE_URL = StringConfig.BASE_URL();


    public static final String BASE_URL(){
        return SharedPreferencesManager.getSharedPrefIP();
    }

    public static final String SHOW_ORDERS = "api/order/show";

    public static final String SERVED_ORDER = "api/order/servedOrder";

    public static final String CANCEL_ORDER = "api/order/cancelOrder";

    public static final String ROUTE_CUSTOMER_INFO = "api/users/show_all_customer";

    //GET ALL FOOD OR DRINK INFO
    public static final String ROUTE_SHOWALL_FOOD = "api/food/show_all";

    public static final String ROUTE_SHOWALL_DRINK = "api/drink/show_all";


    // new routes based on server modifications

    public static final String ROUTE_SHOWORDERS = "api/order/showOrder";

    // ROUTE TO SALES

    public static final String ROUTE_SALES = "api/sale/sales_order";

    // ROUTE TO INVENTORY

    public static final String ROUTE_UPDATE_INVENTORY = "api/inventory/update_inventory";

    // field -- saleType : "Drink"  POST
    public static final String ROUTE_DAILY_SALES = "api/sale/daily_sale";

    // SHARED PREFERENCES
    public static final String SHAREDPREF_LIST_USER = "CBLCAdminUserList";

    public static final String SHAREDPREF_LIST_FOOD_MAIN = "CBLC_FOOD_MAIN";

    public static final String SHAREDPREF_LIST_FOOD_CATEGORY = "CBLC_FOOD_CATEGORY";

    public static final String SHAREDPREF_LIST_FOOD_SERVING = "CBLC_FOOD_SERVING";

    public static final String SHAREDPREF_LIST_DRINK_MAIN = "CBLC_DRINK_MAIN";

    public static final String SHAREDPREF_LIST_DRINK_CATEGORY = "CBLC_DRINK_CATEGORY";

    public static final String SHAREDPREF_LIST_DRINK_SERVING = "CBLC_DRINK_SERVING";

    public static final String SHAREDPREF_ORDERQUEUE_FOOD = "CBLC_ORDERED_QUEUE_FOOD";

    public static final String SHAREDPREF_ORDERQUEUE_DRINK = "CBLC_ORDERED_QUEUE_DRINK";

    public static final String SHAREDPREF_ADMIN = "CBLCAdmin";

}
